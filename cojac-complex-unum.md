---
version: 2
titre: Intégration des nombres complexes et des Unum en Java avec COJAC
abréviation: Cojac-complex-unum
filières:
  - Informatique
nombre d'étudiants: 1
mots-clé: [Cojac, Nombres, Instrumentation, Java, Scientific computing, Unum, Posit]
langue: [E,F]
type de projet: Projet de bachelor
année scolaire: 2020/2021
confidentialité: non
suite: non
---
## Contexte

[COJAC](https://youtu.be/eAy71M34U_I?list=PLHLKWUtT0B7kNos1e48vKhFlGAXR1AAkF) est un utilitaire développé à la HEIA-FR qui instrumente du bytecode Java pour enrichir à l'exécution le comportement numérique d'un programme (détection des overflows et autres anomalies, calcul par intervalles, précision arbitrairement grande, etc).

Contrairement à certains langages, Java n'est pas équipé avec un type standard pour travailler en nombres complexes. Par ailleurs, le modèle des nombres à virgule flottante, si courant en informatique, n'est pas la seule norme de calcul scientifique : une alternative récente a été proposée avec l'approche Unum/Posit défendue par le Prof. Gustafson [1], et dont plusieurs implémentations sont disponibles [2][3].

Ainsi, deux extensions naturelles du modèle arithmétique de Java pourraient être envisagées. L'idée serait de continuer à programmer avec le type `double` usuel, puis de permettre l'exécution du programme dans un mode particulier où on bascule vers un autre comportement numérique. 

## Objectifs

Il s'agit d'offrir via COJAC la fonctionnalité d'un remplacement automatique dans un programme Java, au runtime, des nombres à virgule flottante par deux nouveaux types numériques : 

- nombres complexes : les opérations mathématiques seront redéfinies pour prendre en compte la partie imaginaire du nombre complexe;
- au moins une des variétés d'Unum : il faudra vraisemblablement établir une passerelle JNI vers l'implémentation native en C/C++.

Le comportement attendu pourra être basé sur de nouvelles sortes de [`Wrapper`](https://github.com/Cojac/Cojac/wiki#4---cojac-the-enriching-wrapper), c'est-à-dire que toutes les données de type primitif `double` sont remplacées par des objets. Une piste alternative consisterait à conserver le type `double` en interne tout en réinterprétant la représentation binaire, par exemple pour y "cacher" la partie imaginaire du nombre complexe dans les bits les moins significatifs de la mantisse — une astuce déjà illustrée avec un modèle de calcul par intervalles.

Le but est également de concevoir des programmes de démonstration illustrant le potentiel de l'approche.

L'implémentation sera complétée optionnellement par des mesures de performance.


[1] J. Gustafson et al., ["Beating Floating Point at its Own Game: Posit Arithmetic"](http://www.johngustafson.net/pdfs/BeatingFloatingPoint.pdf)    
[2] https://posithub.org/       
[3] https://gitlab.com/cerlane/SoftPosit


<!---

https://github.com/Cojac/Cojac/wiki
https://youtu.be/eAy71M34U_I?list=PLHLKWUtT0B7kNos1e48vKhFlGAXR1AAkF

`inline code`
[GitHub](http://github.com)

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

*emphasized*, **bold**
Just trying to put a comment...
l'[Alambic Temporel](http://.../alambic/)

$ git add *.md; git commit -m "wip"; git push

Meta données				
titre	Texte	obligatoire	1	Max 255 caractères
Abréviation	Texte	optionnel	1	Max 255 caractères
filières	Choix	obligatoire	1..2	[Informatique ; Télécommunications]
orientations	Choix	optionnel	1..3	[Internet et communication ; Réseaux et sécurité]
langue	Lite de Choix	obligatoire	1	[D;F;E]
professeurs co-superviseurs	Texte	obligatoire	1..2	texte ouvert (Max 255 carcatères)
assistants	Texte	optionnel	0..n	Texte ouvert (Max 255 carcatères)
proposé par étudiant	Texte	optionnel	1	Texte ouvert (Max 255 carcatères)
mandants	Texte	optionnel	1..n	Texte iuvert  + URL
instituts	Choix	optionnel	1..n	[HumanTech; iCoSys; iSIS; ENERGY; ChemTech ;iPrint, iRAP; iTEC, SeSi; TRANSFORM]
confidentialité	Binaire	obligatoire	Oui/Non	[Oui; Non]
réalisation	Choix	optionnel	1..n	[labo;entreprise; étranger; instituts externe]
mots-clé	Liste	optionnel	1..n	Termes séparés par des virgule
nombre d'étudiants	Nombre	obligatoire	1..2	[1; 2]
suite	Binaire	obligatoire	Oui/Non	[Oui; Non]
Markdown				
Contexte	Texte	obligatoire	1	
Objectifs	Texte	obligatoire	1	
Contraintes	Teste	optionel	1	
Extrait du répertoire				
Type de projet	Choix	obligatoire	1	Tiré du répertoire [semestre ; bachelor]
Professeur principal	Choix	Obligatoire		Tiré du répertoire

 ```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/myimage.jpg}
\end{center}
```

-->

